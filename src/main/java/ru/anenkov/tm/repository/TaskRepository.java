package ru.anenkov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.model.Task;

import java.util.*;

public interface TaskRepository extends JpaRepository<Task, String> {


}
