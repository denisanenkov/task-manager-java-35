package ru.anenkov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.model.Task;
import ru.anenkov.tm.repository.ProjectRepository;
import ru.anenkov.tm.repository.TaskRepository;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    ProjectRepository projectRepository;

    @GetMapping("/tasks")
    public String index(Model model) {
        List<Task> tasks = taskRepository.findAll();
        model.addAttribute("tasks", tasks);
        return "task-list";
    }

    @GetMapping("/task/create")
    public String create() {
        taskRepository.save(new Task("new Task" + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        taskRepository.deleteById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(Task task) {
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public String edit(@PathVariable("id") String id, Model model) {
        Task task = taskRepository.findById(id).orElse(null);
        List<Project> projects = projectRepository.findAll();
        model.addAttribute("task", task);
        model.addAttribute("projectList", projects);
        return "task-edit";
    }

}
